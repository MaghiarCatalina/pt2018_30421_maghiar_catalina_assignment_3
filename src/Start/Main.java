package Start;

import Presentation.GUI;

/**
 * The class containing the main method
 * @author Catalina Maghiar
 */
public class Main {
    public static void main(String args[]){
        GUI gui = new GUI();
    }
}
