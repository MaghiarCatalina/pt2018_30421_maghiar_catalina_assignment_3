package JTablesCreate;
import java.lang.reflect.Field;

/**
 * @author Catalina Maghiar
 */
public class Reflection {

    public static String[] retrieveProperties(Object object,String values[]) {
        /**
         * this method uses reflection to access the properties of the object given
         * @param an Object object
         * @return an array of Strings, containing the values corresponding to the object's parameters
         * @exception IllegalArgumentException thrown
         */
            int i=0;
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(object);
                values[i]=(value.toString());
                i++;
                System.out.println(field.getName() + "=" + value);

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return values;
    }

    public static void retrieveFields(Object object, String header[]) {
        /**
         * this method uses reflection to get the fields of the given object
         * @param Object object and an array of strings containing the object's fields names
         * @return No return
         * @exception No exception
         */
        int i = 0;
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            header[i]=field.getName();
            System.out.println(header[i]);
            i++;
        }
        System.out.println(header[0]+"  "+header[1]);
    }

}