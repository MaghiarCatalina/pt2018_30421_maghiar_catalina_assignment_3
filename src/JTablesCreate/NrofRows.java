package JTablesCreate;

import DataAccess.ClientDao;
import Connection.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class contains two methods for getting the number of rows(elements) in the client table and respectively the product table
 * @author Catalina Maghiar
 */
public class NrofRows {
    public NrofRows(){}

    private static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());

    public static int headerElementsCountClient(Object object){
        String countStatementString = "SELECT COUNT(idclient) FROM client";
        int nrhearders=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement countStatement = null;
        ResultSet rs = null;
        try{
            countStatement = dbConnection.prepareStatement(countStatementString);
            rs = countStatement.executeQuery();
            rs.next();
            nrhearders = rs.getInt(1);
        }
        catch (SQLException e){
            LOGGER.log(Level.WARNING,"TableViewAll: headerElementsCount " + e.getMessage());
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(countStatement);
            ConnectionFactory.close(dbConnection);
        }
        return nrhearders;
    }

    public static int headerElementsCountProduct(Object object){
        String countStatementString = "SELECT COUNT(idproduct) FROM product";
        int nrhearders=0;
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement countStatement = null;
        ResultSet rs = null;
        try{
            countStatement = dbConnection.prepareStatement(countStatementString);
            rs = countStatement.executeQuery();
            rs.next();
            nrhearders = rs.getInt(1);
        }
        catch (SQLException e){
            LOGGER.log(Level.WARNING,"TableViewAll: headerElementsCountProduct " + e.getMessage());
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(countStatement);
            ConnectionFactory.close(dbConnection);
        }
        return nrhearders;
    }
}

