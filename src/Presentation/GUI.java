package Presentation;

import DataAccess.ClientDao;
import DataAccess.OrderDao;
import DataAccess.ProductDao;
import Model.Client;
import Model.Order;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Graphical user interface
 * @author Catalina Maghiar
 * @since 29.04.2018
 */
public class GUI {
    private JFrame mainframe;
    private ArrayList<Client> allClientsList;
    private ArrayList<Product>allProductsList;

    public GUI(){
        mainframe = new JFrame();

        JPanel client = new JPanel();
        JPanel product = new JPanel();
        JPanel order = new JPanel();

        //****Client****
        // Insert New Client
        JButton addNewClientButton = new JButton("+");
        JLabel newIdLabel = new JLabel("New client id: ");
        newIdLabel.setBounds(30,50,70,20);
        TextField insertIdTf = new TextField();
        insertIdTf.setBounds(110,50,50,20);
        JLabel newnameLabel = new JLabel("Name: ");
        newIdLabel.setBounds(170,50,50,20);
        TextField insertNameTf = new TextField();
        insertIdTf.setBounds(250,50,70,20);

        addNewClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client cl_add = new Client();
                cl_add.setIdclient(Integer.parseInt(insertIdTf.getText()));
                cl_add.setClient_name(insertNameTf.getText());
                int check_new = ClientDao.insert(cl_add);
            }
        });
        client.add(addNewClientButton);
        client.add(newIdLabel);
        client.add(insertIdTf);
        client.add(newnameLabel);
        client.add(insertNameTf);

        // Edit client
        JButton editClientButton = new JButton("Edit");
        editClientButton.setBounds(5,70,70,20);
        JLabel oldIdLabel = new JLabel("Old id: ");
        oldIdLabel.setBounds(30,50,70,20);
        TextField oldIdTf = new TextField();
        oldIdTf.setBounds(110,50,50,20);
        JLabel idEditLabel = new JLabel("New id: ");
        idEditLabel.setBounds(170,50,50,20);
        TextField idEditTf = new TextField();
        idEditTf.setBounds(250,50,70,20);
        JLabel nameEditLabel = new JLabel("New name: ");
        nameEditLabel.setBounds(170,50,50,20);
        TextField nameEditTf = new TextField();
        nameEditTf.setBounds(250,50,70,20);
        editClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idtoedit = Integer.parseInt(oldIdTf.getText());
                Client cl = new Client(Integer.parseInt(idEditTf.getText()),nameEditTf.getText());
                ClientDao.edit(cl,idtoedit);
            }
        });
        client.add(editClientButton);
        client.add(oldIdLabel);
        client.add(oldIdTf);
        client.add(idEditLabel);
        client.add(idEditTf);
        client.add(nameEditLabel);
        client.add(nameEditTf);

        //Delete Client
        JButton deleteClientButton = new JButton("Delete");
        deleteClientButton.setBounds(5,70,70,20);
        JLabel idDeleteLabel = new JLabel("Delete id: ");
        idDeleteLabel.setBounds(30,50,70,20);
        TextField idDeleteTf = new TextField();
        idDeleteTf.setBounds(110,50,50,20);
        deleteClientButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client cl_delete = new Client();
                cl_delete=ClientDao.findById(Integer.parseInt(idDeleteTf.getText()));
                ClientDao.delete(cl_delete);
            }
        });
        client.add(deleteClientButton);
        client.add(idDeleteLabel);
        client.add(idDeleteTf);

        // View All Clients
        JButton viewAllClients = new JButton("View All");
        viewAllClients.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                allClientsList = ClientDao.viewall();
                JTable jt= new JTable();
                jt= ClientDao.createTable(allClientsList);
                Frame ftable = new Frame();
                ftable.setVisible(true);
                ftable.setSize(300,300);
                jt.setAutoscrolls(true);
                jt.setEnabled(true);
                JScrollPane sp2 = new JScrollPane(jt);
                ftable.add(sp2);
            }
        });
        client.add(viewAllClients);
        Button cancel =new Button("Exit");
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        client.add(cancel);
        client.setLayout(new FlowLayout(FlowLayout.LEFT));

        //*****Product
        //New Product
        JButton addNewProductButton = new JButton("+");
        JLabel newIdproduct = new JLabel("New product id: ");
        TextField insertIdTfproduct = new TextField();
        JLabel newnameLabelproduct = new JLabel("Name: ");
        TextField insertNameTfProduct = new TextField();
        JLabel quantityLabel = new JLabel("Quantity: ");
        TextField quantityTf = new TextField();
        JLabel priceLabel = new JLabel("Price: ");
        TextField priceTf = new TextField();

        addNewProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product p_add = new Product();
                p_add.setIdproduct(Integer.parseInt(insertIdTfproduct.getText()));
                p_add.setProduct_name(insertNameTfProduct.getText());
                p_add.setQuantity(Integer.parseInt(quantityTf.getText()));
                p_add.setPrice(Integer.parseInt(priceTf.getText()));
                int check_new = ProductDao.insert(p_add);
            }
        });

        product.add(addNewProductButton);
        product.add(newIdproduct);
        product.add(insertIdTfproduct);
        product.add(newnameLabelproduct);
        product.add(insertNameTfProduct);
        product.add(quantityLabel);
        product.add(quantityTf);
        product.add(priceLabel);
        product.add(priceTf);

        // Edit product
        JButton editProductButton = new JButton("Edit");
        JLabel oldIdLabelProduct = new JLabel("Old id: ");
        TextField oldIdTfProduct = new TextField();
        JLabel idProductL = new JLabel("New id: ");
        TextField idProductTf = new TextField();
        JLabel nameProductL = new JLabel("New name: ");
        TextField nameProductTf = new TextField();
        JLabel quantityProductL = new JLabel("Quantity: ");
        TextField quantityProductTf = new TextField();
        JLabel priceProductL = new JLabel("Price: ");
        TextField priceProductTf = new TextField();
        editProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               int idold = Integer.parseInt(oldIdTfProduct.getText());
               Product pr = new Product(Integer.parseInt(idProductTf.getText()),nameProductTf.getText(),Integer.parseInt(quantityProductTf.getText()),Integer.parseInt(priceProductTf.getText()));
               ProductDao.edit(pr,idold);
            }
        });

        product.add(editProductButton);
        product.add(oldIdLabelProduct);
        product.add(oldIdTfProduct);
        product.add(idProductL);
        product.add(idProductTf);
        product.add(nameProductL);
        product.add(nameProductTf);
        product.add(quantityProductL);
        product.add(quantityProductTf);
        product.add(priceProductL);
        product.add(priceProductTf);

        //Delete Product
        JButton deleteProductButton = new JButton("Delete");
        JLabel idDeleteProductL = new JLabel("Delete id: ");
        TextField idDeleteProductTf = new TextField();
        deleteProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product p_delete = new Product();
                p_delete=ProductDao.findById(Integer.parseInt(idDeleteProductTf.getText()));
                ProductDao.delete(p_delete);
            }
        });
        product.add(deleteProductButton);
        product.add(idDeleteProductL);
        product.add(idDeleteProductTf);

        // View All Products
        JButton viewAllProducts = new JButton("View All");
        viewAllProducts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                allProductsList = ProductDao.viewall();
                JTable jt= new JTable();
                jt= ProductDao.createTable(allProductsList);
                Frame ftable = new Frame();
                ftable.setVisible(true);
                ftable.setSize(300,300);
                jt.setAutoscrolls(true);
                jt.setEnabled(true);
                JScrollPane sp = new JScrollPane(jt);
                ftable.add(sp);
            }
        });
        product.add(viewAllProducts);
        Button cancelProduct =new Button("Exit");
        cancelProduct.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        product.add(cancelProduct);
        product.setLayout(new FlowLayout(FlowLayout.LEFT));

        //*****Order

        JButton orderButton = new JButton("Order");
        JLabel orderIdL = new JLabel("Order id: ");
        TextField orderIDTf = new TextField();
        JLabel clientOrderID = new JLabel("Client id: ");
        TextField clientOrderIDTf = new TextField();
        JLabel productOrderID = new JLabel("Product id: ");
        TextField productOrderIDTf = new TextField();
        JLabel nrofItemsL = new JLabel("Number of items: ");
        TextField nrofItemsTf = new TextField();
        JLabel totalL = new JLabel("Total: ");
        TextField totalTf = new TextField();
        totalTf.setEditable(false);
        JButton bill = new JButton("Make bill");
        JButton cancelOrder = new JButton("Exit");

        cancelOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(1);
            }
        });
        orderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idprod = Integer.parseInt(productOrderIDTf.getText());
                int howManyItems = Integer.parseInt(nrofItemsTf.getText());
                int topay=0;
                int insertOrder;
                Product wanted= ProductDao.findById(idprod);
                if(howManyItems>wanted.getQuantity()){
                    JOptionPane.showMessageDialog(null,"Under stock");
                }
                else{
                    topay = (Integer.parseInt(nrofItemsTf.getText()))*(wanted.getPrice());
                    System.out.println(topay);
                    int id_o = Integer.parseInt(orderIDTf.getText());
                    int id_c = Integer.parseInt(clientOrderIDTf.getText());
                    int id_p = Integer.parseInt(productOrderIDTf.getText());
                    int q = Integer.parseInt(nrofItemsTf.getText());
                    Order order = new Order(id_o,id_c,id_p,q,topay);
                    insertOrder=OrderDao.insert(order);
                    String showTotal = String.valueOf(topay);
                    totalTf.setText(showTotal);
                    wanted.setQuantity(wanted.getQuantity()-howManyItems);
                    ProductDao.update(wanted);
                }
            }
        });
        bill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int idorder = Integer.parseInt(orderIDTf.getText());
                Order o = OrderDao.findById(idorder);
                OrderDao.makeBill(o);
            }
        });

        order.add(orderButton);
        order.add(orderIdL);
        order.add(orderIDTf);
        order.add(clientOrderID);
        order.add(clientOrderIDTf);
        order.add(productOrderID);
        order.add(productOrderIDTf);
        order.add(nrofItemsL);
        order.add(nrofItemsTf);
        order.add(totalL);
        order.add(totalTf);
        order.add(bill);
        order.add(cancelOrder);
        order.setLayout(new FlowLayout(FlowLayout.LEFT));

        JTabbedPane tabs = new JTabbedPane();
        tabs.setBounds(0,0,500,500);
        tabs.add("Clients",client);
        tabs.add("Products",product);
        tabs.add("Order",order);

        mainframe.add(tabs);
        mainframe.setTitle("Order Management");
        mainframe.setSize(500,500);
        mainframe.setVisible(true);

    }
}
