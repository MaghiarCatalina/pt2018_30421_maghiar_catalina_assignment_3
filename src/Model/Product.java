package Model;

/**
 *This class has the same parameters as the table product from the database
 * It contains getters and setters for each parameter and a method toString
 *  * @author Catalina Maghiar
 */
public class Product {
    private int idproduct;
    private String product_name;
    private int quantity;
    private int price;
    public Product(){}
    public Product(int id,String product_name,int quantity,int price){
        this.idproduct=id;
        this.product_name=product_name;
        this.quantity=quantity;
        this.price=price;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public String getProduct_name() {
        return product_name;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product: id:"+idproduct+"  name:"+product_name+"  quantity:"+quantity+"  price"+price;
    }
}
