package Model;

/**
 *This class has the same parameters as the table order from the database
 * It contains getters and setters for each parameter and a method toString
 *  * @author Catalina Maghiar
 */
public class Order {
    private int idorder;
    private int id_client;
    private int id_product;
    private int nr_of_items;
    private int total_price;
    public Order(){}
    public Order(int idorder,int id_client,int id_product,int nr_of_items, int total_price){

        this.idorder=idorder;
        this.id_client=id_client;
        this.id_product=id_product;
        this.nr_of_items=nr_of_items;
        this.total_price=total_price;
    }
    public Order(int idorder,int id_client,int id_product){
        super();
        this.idorder=idorder;
        this.id_client=id_client;
        this.id_product=id_product;
        this.nr_of_items=1;
        this.total_price=0;
    }

    public int getIdorder() {
        return idorder;
    }

    public int getId_client() {
        return id_client;
    }

    public int getId_product() {
        return id_product;
    }

    public int getNr_of_items() {
        return nr_of_items;
    }

    public int getTotal_price() {
        return total_price;
    }

    @Override
    public String toString() {
        return "Order:"+"\n"+ "id order: "+idorder+" client id: "+id_client+" product id: "+id_product+"\n"+"Number of items: "+nr_of_items+"\n"+"Total price: "+total_price;
    }
}
