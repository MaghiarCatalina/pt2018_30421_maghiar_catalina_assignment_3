package Model;

/**
 * This class has the same parameters as the table client from the database
 * It contains getters and setters for each parameter and a method toString
 * @author Catalina Maghiar
 */
public class Client {
    private int idclient;
    private String client_name;
    public Client(){}
    public Client(int idclient,String client_name){
        this.idclient=idclient;
        this.client_name=client_name;
    }
    public Client(int idclient){
        super();
        this.idclient=idclient;
    }

    public int getIdclient() {
        return idclient;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public void setIdclient(int idclient) {
        this.idclient = idclient;
    }

    @Override
    public String toString() {
        return "Client: id: "+idclient+ "  name:"+client_name;
    }
}
