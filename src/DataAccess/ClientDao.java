package DataAccess;

import JTablesCreate.Reflection;
import JTablesCreate.NrofRows;
import Model.Client;
import Connection.ConnectionFactory;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides the data access to the client table
 * @author Catalina Maghiar
 */
public class ClientDao {
    private static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
    private static final String insertStatementString = "INSERT INTO client (idclient,client_name)"
            + " VALUES (?,?)";
    private final static String findStatementString = "SELECT * FROM client where idclient = ?";
    private final static String deleteStatementString = "DELETE FROM client WHERE idclient= ?";
    private final static String viewallStatementString = "SELECT * FROM client";
    private final static String editStatementString = "UPDATE client SET idclient=?, client_name=? where idclient=?";

    public static Client findById(int idclient) {
        /**
         * This method receives an id and executes a query for finding the row with the corresponding id
         * @param int idclient
         * @return Client
         * @exception SQLException
         */
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setInt(1, idclient);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("client_name");
            toReturn = new Client(idclient, name);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Client client) {
        /**
         * This method receives a Client and executes a query for inserting a row with the corresponding information
         * @param Client client
         * @return int
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, client.getIdclient());
            insertStatement.setString(2, client.getClient_name());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDao:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
    public static void delete(Client client){
        /**
         * This method receives a Client and executes a query for deleting the row with the corresponding information
         * @param Client client
         * @return No return
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setInt(1, client.getIdclient());
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDao:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }

    }
    public static ArrayList<Client> viewall (){
        /**
         * This method executes a query for finding all the row in the client table and puts them into an ArrayList of Clients
         * @param No parameter
         * @return ArrayList<Client>
         * @exception Exception
         */
        ArrayList<Client> clientslist = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement viewStatement = null;
        ResultSet rs = null;
        try{
            viewStatement = dbConnection.prepareStatement(viewallStatementString, Statement.RETURN_GENERATED_KEYS);
            rs = viewStatement.executeQuery();
            while (rs.next()){
                Client toadd = new Client(rs.getInt(1),rs.getString(2));
                clientslist.add(toadd);
            }
        }
        catch (Exception e){
            LOGGER.log(Level.WARNING, "ClientDao:view all " + e.getMessage());
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(viewStatement);
            ConnectionFactory.close(dbConnection);
        }
        return clientslist;
    }
    public static JTable createTable(ArrayList<Client> clientArrayList){
        String headers[] =new String[2];
        String val[]=new String[2];
        Reflection.retrieveFields(clientArrayList.get(0),headers);
        int nrrows = NrofRows.headerElementsCountClient(clientArrayList.get(0));
        String data[][] = new String[nrrows][2];
        for(int j=0;j<clientArrayList.size();j++){
            Reflection.retrieveProperties(clientArrayList.get(j),val);
            data[j][0]=val[0];
            data[j][1]=val[1];
        }
        JTable jt = new JTable(data,headers);
        return jt;
   }

    public static void edit(Client client,int oldid){
        /**
         * This method receives a Client and it's old id
         * It executes a query to update the corresponding row
         * @param int, Client
         * @return No return
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement editStatement = null;
        try {
            editStatement = dbConnection.prepareStatement(editStatementString, Statement.RETURN_GENERATED_KEYS);
            editStatement.setInt(1, client.getIdclient());
            editStatement.setString(2,client.getClient_name());
            editStatement.setInt(3,oldid);
            editStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDao:edit " + e.getMessage());
        } finally {
            ConnectionFactory.close(editStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}
