package DataAccess;

import Model.Client;
import Model.Order;
import Connection.ConnectionFactory;
import Model.Product;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides the data access to the order table
 * @author Catalina Maghiar
 */

public class OrderDao {
    private static final Logger LOGGER = Logger.getLogger(OrderDao.class.getName());
    private static final String insertStatementString = "INSERT INTO ordertable (idorder,id_client,id_product,nr_of_items,total_price)"
            + " VALUES (?,?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM ordertable where idorder = ?";

    public static int insert(Order order) {
        /**
         * This method receives an Order and executes a query for inserting the row with the corresponding data
         * @param Order order
         * @return int
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, order.getIdorder());
            insertStatement.setInt(2, order.getId_client());
            insertStatement.setInt(3,order.getId_product());
            insertStatement.setInt(4,order.getNr_of_items());
            insertStatement.setInt(5,order.getTotal_price());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void makeBill(Order order){
        /**
         * This method receives an Order
         * Creates a file in which are written information about the order
         * @param Order order
         * @return No return
         * @exception Exception e
         */
        Product p = ProductDao.findById(order.getId_product());
        Client c= ClientDao.findById(order.getId_client());
        List<String> content = Arrays.asList("Product name: ",p.getProduct_name(),"Client name: ",c.getClient_name(),order.toString());
        try {
            Path file = Paths.get("Order " + order.getIdorder() + ".txt");
            Files.write(file, content, Charset.forName("UTF-8"));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Order findById(int idorder) {
        /**
         * This method receives an id and executes a query for finding the row with the corresponding data
         * @param int idorder
         * @return Order
         * @exception SQLException
         */
        Order toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setInt(1, idorder);
            rs = findStatement.executeQuery();
            rs.next();
            int id_o = rs.getInt("idorder");
            int id_c = rs.getInt("id_client");
            int id_p = rs.getInt("id_product");
            int nritems = rs.getInt("nr_of_items");
            int price = rs.getInt("total_price");

            toReturn = new Order(id_o,id_c,id_p, nritems,price);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"OrderDao:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }
}
