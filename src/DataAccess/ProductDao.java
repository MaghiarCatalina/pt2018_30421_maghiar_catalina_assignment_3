package DataAccess;

import JTablesCreate.Reflection;
import JTablesCreate.NrofRows;
import Model.Product;
import Connection.ConnectionFactory;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides the data access to the product table
 * @author Catalina Maghiar
 */
public class ProductDao {
    private static final Logger LOGGER = Logger.getLogger(ProductDao.class.getName());
    private static final String insertStatementString = "INSERT INTO product (idproduct,product_name,quantity,price)"
            + " VALUES (?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM product where idproduct = ?";
    private final static String deleteStatementString = "DELETE FROM product WHERE idproduct= ?";
    private final static String viewallStatementString = "SELECT * FROM product";
    private static final String updateStatementString = "UPDATE product SET quantity=? where idproduct=?";
    private static final String editStatementString = "UPDATE product SET idproduct=?, product_name=?, quantity=?, price=? where idproduct=?";

    public static Product findById(int idproduct) {
        /**
         * This method receives an id and executes a query for finding the row with the corresponding id
         * @param int idproduct
         * @return Product
         * @exception SQLException
         */
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setInt(1, idproduct);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("product_name");
            int quantity = rs.getInt("quantity");
            int price = rs.getInt("price");
            toReturn = new Product(idproduct, name,quantity,price);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Product product) {
        /**
         * This method receives a Product and executes a query for inserting a row with the corresponding info
         * @param Product product
         * @return int
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, product.getIdproduct());
            insertStatement.setString(2, product.getProduct_name());
            insertStatement.setInt(3,product.getQuantity());
            insertStatement.setInt(4,product.getPrice());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDao:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void delete(Product product){
        /**
         * This method receives a Product and executes a query for deleting the row with the corresponding data
         * @param Product product
         * @return No return
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setInt(1, product.getIdproduct());
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDao: delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public static void update(Product product){
        /**
         * This method receives a Product and executes a query for updating the row with the corresponding data
         * @param Product product
         * @return No return
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement updateStatement = null;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setInt(1, product.getQuantity());
            updateStatement.setInt(2,product.getIdproduct());
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDao:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static ArrayList<Product> viewall (){
        /**
         * This method executes a query and retrieves all the rows in the product table as an ArrayList of Products
         * @param no param
         * @return ArrayLIst<Product>
         * @exception Exception
         */
        ArrayList<Product> productArrayList = new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement viewStatement = null;
        ResultSet rs = null;
        try{
            viewStatement = dbConnection.prepareStatement(viewallStatementString, Statement.RETURN_GENERATED_KEYS);
            rs = viewStatement.executeQuery();
            while (rs.next()){
                Product toadd = new Product(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4));
                productArrayList.add(toadd);
            }
        }
        catch (Exception e){
            LOGGER.log(Level.WARNING, "ProductDao:view all " + e.getMessage());
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(viewStatement);
            ConnectionFactory.close(dbConnection);
        }
        return productArrayList;
    }

    public static JTable createTable(ArrayList<Product> productArrayList){
        /**
         * This method receives an ArrayList of products and creates a JTable with all of them
         * @param ArrayList<Product>
         * @return JTable
         * @exception no Exception
         */
        String header[] = new String[4];
        String val[]=new String[4];
        Reflection.retrieveFields(productArrayList.get(0),header);
        int nrrows = NrofRows.headerElementsCountProduct(productArrayList.get(0));
        String data[][] = new String[nrrows][4];
        for(int j=0;j<productArrayList.size();j++){
            Reflection.retrieveProperties(productArrayList.get(j),val);
            data[j][0]=val[0];
            data[j][1]=val[1];
            data[j][2]=val[2];
            data[j][3]=val[3];
        }
        JTable jt = new JTable(data,header);
        return jt;
    }

    public static void edit(Product product, int oldid){
        /**
         * This method receives an id and a Product and executes a query for updating the changes made to a row in the table
         * @param int oldid
         * @return no return
         * @exception SQLException
         */
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement editStatement = null;
        try {
            editStatement = dbConnection.prepareStatement(editStatementString, Statement.RETURN_GENERATED_KEYS);
            editStatement.setInt(1, product.getIdproduct());
            editStatement.setString(2,product.getProduct_name());
            editStatement.setInt(3,product.getQuantity());
            editStatement.setInt(4,product.getPrice());
            editStatement.setInt(5,oldid);
            editStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDao:edit " + e.getMessage());
        } finally {
            ConnectionFactory.close(editStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

}
